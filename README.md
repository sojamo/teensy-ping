# Ping Sensors using a Teensy 3.2

This repo contains the source code for running 3 Ping sensors and a TLC5940 simultaneously on a Teensy 3.2. The program makes use of 3 libraries that need to be copied into the lib folder. 

  - [New Ping](https://bitbucket.org/teckel12/arduino-new-ping/downloads)
  - [Serialindex](https://github.com/aqiank/serialindex-arduino/tree/dev) currently under development, also check the master branch for stable release.
  - [TLC5940](https://github.com/PaulStoffregen/Tlc5940)

When running this setup, the teensy will send changes through a Serial connection. The data as a string-formatted  array and follows the Serialindex format.


## Compiling and upload

This project uses the platformio environment to compile and upload the source code to a teensy, use 

```
platformio run -t upload 
```

to compile and upload code.


