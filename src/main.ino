/*
   Basic Pin setup:

             ---u----
       OUT1 |1     28| OUT channel 0
       OUT2 |2     27|-> GND (VPRG)
       OUT3 |3     26|-> SIN (Teensy3.2 pin 6)
       OUT4 |4     25|-> SCLK (Teensy3.2 pin 7)
         .  |5     24|-> XLAT (Teensy3.2 pin 3)
         .  |6     23|-> BLANK (Teensy3.2 pin 4) + 10K Resistor to 5V
         .  |7     22|-> GND
         .  |8     21|-> VCC (+5V)
         .  |9     20|-> 2K Resistor -> GND
         .  |10    19|-> +5V (DCPRG)
         .  |11    18|-> GSCLK (Teensy3.2 pin 5)
         .  |12    17|-> SOUT
         .  |13    16|-> XERR
       OUT14|14    15| OUT channel 15
             ---------

  References:
    https://www.pjrc.com/teensy/td_libs_Tlc5940.html for how to wire TLC5940 to Teensy
    https://www.pjrc.com/teensy/pinout.html for Teensy 3.2 pin layout
    TLC5940 library basic use example for TLC5940 pin layout

*/
#include <NewPing.h> //Use the latest version uploaded 30 Jul 2016, not the Teensy library version
#include <Tlc5940.h>
#include <SerialIndex.h>

#define SONAR_NUM 3 //number of sensors
#define MAX_DISTANCE 400 //max detection range in cm
#define PING_INTERVAL 20 //milliseconds between pings

unsigned long pingTimer[SONAR_NUM]; //tracks time for each sensor's ping
int cm[SONAR_NUM];//stores ping distances
uint8_t currentSensor = 0; //which sensor is active

NewPing sonar[SONAR_NUM] = {
  NewPing(14, 14, MAX_DISTANCE), //trigger pin, echo pin, max sensing dist
  NewPing(15, 15, MAX_DISTANCE),
  NewPing(16, 16, MAX_DISTANCE)
};

int direction = 1; //for the LED fade effect
float brightLevelFloat; //brightness of the LED
int brightLevelInt; //brightness of the LED

SerialIndex s1;

void setup() {
  s1.setSerial(Serial).read(true).write(true).add("cm",cm,4);
  Tlc.init();

  pingTimer[0] = millis() + 75; //first ping start in ms
  for (uint8_t i = 1; i < SONAR_NUM; i++) {
    pingTimer[i] = pingTimer[i - 1] + PING_INTERVAL;
  }
}

void loop() {
  read_pings();
  fade_oneLED();
  delay(10);
  s1.update();
}

void fade_oneLED() {

  brightLevelFloat += direction*10;
  brightLevelInt = round(brightLevelFloat);

  Tlc.set(0, brightLevelInt);

  if (brightLevelInt <= 0) {
    direction *= 1;
  } else if (brightLevelInt >= 4000) { //max brightness
    direction *= -1;
  }
  Tlc.update();
}

void read_pings() {

  for (uint8_t i = 0; i < SONAR_NUM; i++) {
    if (millis() >= pingTimer[i]) {
      pingTimer[i] += PING_INTERVAL * SONAR_NUM;
      sonar[currentSensor].timer_stop();
      currentSensor = i;
      cm[currentSensor] = 0;
      sonar[currentSensor].ping_timer(echoCheck); //send a ping and call a function to test if ping is complete
    }
  }
}

void echoCheck() { //if ping returns, update the distance array
  if (sonar[currentSensor].check_timer()) //ping has returned
    cm[currentSensor] = sonar[currentSensor].ping_result / US_ROUNDTRIP_CM;
}
